import { render, screen } from "@testing-library/react";
import { Logo } from "..";

describe('[Logo] - Componente', () => {

    it('Render Logo component', () => {

        const nombre = 'Simón';
        render(<Logo name={nombre} />);
        const selector = screen.getByText(nombre, {exact: false});

        expect(selector).toBeInTheDocument();
    })

    it('Render subtitle <h2>', () => {

        render(<Logo name="Simón"/>);
        const selector = screen.getByTestId('subtitle');

        expect(selector).toBeDefined();
    })

});
