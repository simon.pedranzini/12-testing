import { render, screen } from '@testing-library/react'
import SayMyName from './SayMyName';

describe('[TEST] - Componente SayMyName', () => {

    it('Buscar "Simón" en el componente', () => {

        //ARANGE
        const textToFind = 'Simón';
        //ACT
        render(<SayMyName/>);
        //ASSERT
        expect(screen.getByText(textToFind, {exact: false})).toBeInTheDocument()
    });
})